<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creation of 50 users
        factory('App\User', 50)->create();

        // Creation of 1 moderator
        factory('App\User')->create([
            'username' => 'modo',
            'nickname' => 'supermodo',
            'password' => Hash::make('modo'),
            'is_deleted' => false,
            'role_id' => Role::where('name', 'moderator')->first()->id
        ]);

        // Creation of 1 administrator
        factory('App\User')->create([
            'username' => 'admin',
            'nickname' => 'dieux',
            'password' => Hash::make('admin'),
            'is_deleted' => false,
            'role_id' => Role::where('name', 'administrator')->first()->id
        ]);
    }
}
