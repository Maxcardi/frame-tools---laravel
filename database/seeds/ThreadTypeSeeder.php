<?php

use Illuminate\Database\Seeder;

class ThreadTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\ThreadType', 5)->create();
    }
}
