<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\Role')->create();
        factory('App\Models\Role')->create([
            'value' => 1,
            'name' => 'moderator',
            'label' => 'Moderateur'
        ]);
        factory('App\Models\Role')->create([
            'value' => 2,
            'name' => 'administrator',
            'label' => 'administrateur'
        ]);
    }
}
