<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    $bool = [true, false];

    return [
        'user_id' => random_int(1, 50),
        'title' => $faker->word,
        'description' => $faker->text(10),
        'is_deleted' => $bool[random_int(0,1)]
    ];
});
