<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Message::class, function (Faker\Generator $faker) {
    $bool = [true, false];

    return [
        'message_id' => null,
        'thread_id' => random_int(1, 5),
        'user_id' => random_int(1, 50),
        'body' => $faker->paragraph(5),
        'is_deleted' => $bool[random_int(0,1)],
    ];
});
