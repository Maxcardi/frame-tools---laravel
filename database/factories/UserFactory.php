<?php

use App\Models\Role;
use Illuminate\Support\Facades\Hash;

/**
 * Factory for the users. It generate a user with random values.
 */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email, // Simulate an email
        'username' => $faker->name, // Simulate a name
        'nickname' => $faker->name, // Simulate a name
        'is_deleted' => $faker->boolean, // Simulate a existant or deleted user
        'password' => Hash::make($faker->password), // Simulate a hashed password for the user
        'role_id' => Role::where('name', 'user')->first()->id // Every user created by the factory is a "user"
    ];
});
