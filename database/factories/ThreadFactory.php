<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Thread::class, function (Faker\Generator $faker) {
    $bool = [true, false];
    
    return [
        'thread_type_id' => random_int(1, 5),
        'category_id' => random_int(1, 5),
        'role_id' => random_int(1, 3),
        'user_id' => random_int(1, 10),
        'title' => $faker->word,
        'description' => $faker->sentence(10),
        'is_opened' => $bool[random_int(0,1)],
        'is_deleted' => $bool[random_int(0,1)]
    ];
});
