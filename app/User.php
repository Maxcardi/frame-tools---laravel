<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'username', 'password', 'email', 'nickname', 'is_deleted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The role of the user
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * The categories of the user
     */
    public function categories()
    {
        return $this->hasMany('App\Models\Role');
    }

    /**
     * The threads of the user
     */
    public function thread()
    {
        return $this->hasMany('App\Models\Thread');
    }

    /**
     * The messages of the user
     */
    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }
}
