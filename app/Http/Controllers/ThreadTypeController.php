<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ThreadType;

class ThreadTypeController extends Controller
{

    public function index()
    {
        $threadtypes = ThreadType::all();

        return $threadtypes;
    }

    public function store(Request $request)
    {
        $threadtype = ThreadType::create([
            'name' => $request->name,
            'label' => $request->label,
            'value' => $request->value,
        ]);

        return $threadtype;
    }

    public function show($threadtype)
    {
        $threadtype = ThreadType::find($threadtype);

        return $threadtype;
    }

    public function update(Request $request, $threadtype)
    {
        $threadtype = ThreadType::find($threadtype);
        $threadtype->name = $request->name;
        $threadtype->label = $request->label;
        $threadtype->value = $request->value;
        $threadtype->save();

        return $threadtype;
    }

    public function delete($threadtype)
    {
        $threadtype = ThreadType::find($threadtype);
        $threadtype->delete();

        return response()->json([
            'message' => 'Thread type successfully deleted',
        ]);
    }

    public function threads($threadtype)
    {
        $threadtype = ThreadType::find($threadtype);
        $threads = $threadtype->threads;

        return $threads;
    }
}
