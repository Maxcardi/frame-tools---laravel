<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;

class MessageController extends Controller
{

    public function index()
    {
        $messages = Message::all();

        return $messages;
    }

    public function store(Request $request)
    {
        $message = Message::create([
            'message_id' => $request->message_id,
            'thread_id' => $request->thread_id,
            'user_id' => $request->user_id,
            'body' => $request->body,
            'is_deleted' => $request->is_deleted
        ]);

        return $message;
    }

    public function show($message)
    {
        $message = Message::find($message);

        return $message;
    }

    public function update(Request $request, $message)
    {
        $message = Message::find($message);
        $message->message_id = $request->message_id;
        $message->thread_id = $request->thread_id;
        $message->user_id = $request->user_id;
        $message->body = $request->body;
        $message->is_deleted = $request->is_deleted;
        $message->save();

        return $message;
    }

    public function delete($message)
    {
        $message = Message::find($message);
        $message->delete();

        return response()->json([
            'message' => 'Message successfully deleted',
        ]);
    }

    public function messages($message)
    {
        $message = Message::find($message);
        $messages = $message->messages();

        return $messages;
    }
}
