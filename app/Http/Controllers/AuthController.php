<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

class AuthController extends Controller
{
    /**Return the connected user */
    public function user(Request $request) {
        return $request->user();
    }

    /**Register a user in the application and connect him.
     * Return the token to use to access the protected routes*/
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $request['password'] = Hash::make($request['password']);
        $user = User::create($request->toArray());
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token, 'user' => $user];
        return response($response, 200);
    }

    /**Log in a user using an email and a password */
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token, 'user' => $user];
                return response($response, 200);
            } 
            else {
                $response = "Invalid credentials";
                return response($response, 422);
            }
        } 
        else {
            $response = 'Invalid credentials';
            return response($response, 422);
        }
    }

    /**Log out a user corresponding to the token in parameters */
    public function logout (Request $request)
    {
        $token = $request->user()->token();
        $token->each(function($token, $key) {
            $token->delete();
        });
        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    }
}
