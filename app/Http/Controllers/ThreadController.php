<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thread;

class ThreadController extends Controller
{

    public function index()
    {
        $threads = Thread::all();

        return $threads;
    }

    public function store(Request $request)
    {
        $thread = Thread::create([
            'thread_type_id' => $request->thread_type_id,
            'category_id' => $request->category_id,
            'role_id' => $request->role_id,
            'user_id' => $request->user_id,
            'title' => $request->title,
            'description' => $request->description,
            'is_opened' => $request->is_opened,
            'is_deleted' => $request->is_deleted
        ]);

        return $thread;
    }

    public function show($thread)
    {
        $thread = Thread::find($thread);

        return $thread;
    }

    public function update(Request $request, $thread)
    {
        $thread = Thread::find($thread);
        $thread->thread_type_id = $request->thread_type_id;
        $thread->category_id = $request->category_id;
        $thread->role_id = $request->role_id;
        $thread->user_id = $request->user_id;
        $thread->title = $request->title;
        $thread->description = $request->description;
        $thread->is_opened = $request->is_opened;
        $thread->is_deleted = $request->is_deleted;
        $thread->save();

        return $thread;
    }

    public function delete($thread)
    {
        $thread = Thread::find($thread);
        $thread->delete();

        return response()->json([
            'message' => 'Thread successfully deleted',
        ]);
    }

    public function messages($thread)
    {
        $thread = Thread::find($thread);
        $messages = [];
        
        foreach ($thread->messages as $key => $message) {
            $message['user'] = $message->user;
            array_push($messages, $message);
        }

        return $messages;
    }
}
