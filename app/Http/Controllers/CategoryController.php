<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**Get every categories */
    public function index()
    {
        $categories = Category::all();

        return $categories;
    }

    /**Add a category */
    public function store(Request $request)
    {
        $category = Category::create([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'description' => $request->description,
            'is_deleted' => $request->is_deleted
        ]);

        return $category;
    }

    /**Get one category */
    public function show($category)
    {
        $category = Category::find($category);

        return $category;
    }

    /**Update a category */
    public function update(Request $request, $category)
    {
        $category = Category::find($category);
        $category->user_id = $request->user_id;
        $category->title = $request->title;
        $category->description = $request->description;
        $category->is_deleted = $request->is_deleted;
        $category->save();

        return $category;
    }

    /**Delete a category */
    public function delete($category)
    {
        $category = Category::find($category);
        $category->delete();

        return response()->json([
            'message' => 'Category successfully deleted',
        ]);
    }

    /**Get every threads for a category */
    public function threads($category)
    {
        $category = Category::find($category);
        $threads = [];

        
        foreach ($category->threads as $key => $thread) {
            $thread['user'] = $thread->user;
            array_push($threads, $thread);
        }

        return $threads;
    }
}
