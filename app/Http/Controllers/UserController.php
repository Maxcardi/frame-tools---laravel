<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $users = User::all();
        return $users;
    }

    public function show($id)
    {
        $user = User::find($id);
        return $user;
    }

    public function store(Request $request)
    {
        $user = User::create([
            'email' => $request->email,
            'username' => $request->firstname,
            'is_deleted' => false,
            'password' => Hash::make($request->password),
            'role_id' => $request->user_type_id
        ]);

        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->email = $request->email;
        $user->username = $request->username;
        $user->is_deleted = $request->is_deleted;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;

        $user->save();

        return $user;
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json([
                "message" => "User successfully deleted"
            ], 
            200);
    }
    //
}
