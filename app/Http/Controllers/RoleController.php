<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $roles = Role::all();
        return $roles;
    }

    public function show($id)
    {
        $role = Role::find($id);
        return $role;
    }

    public function store(Request $request)
    {
        $role = Role::create([
            'value' => $request->value,
            'name' => $request->name,
            'label' => $request->label
        ]);

        return $role;
    }

    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $role->value = $request->value;
        $role->name = $request->name;
        $role->label = $request->label;

        $role->save();

        return $role;
    }

    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        return response()->json([
                "message" => "role successfully deleted"
            ], 
            200);
    }
}
