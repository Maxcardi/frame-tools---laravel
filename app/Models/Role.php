<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'name', 'label',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }
    
    public function threads()
    {
        return $this->hasMany('App\Models\Role');
    }
}
