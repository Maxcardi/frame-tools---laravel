<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'is_deleted',
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function threads()
    {
        return $this->hasMany('App\Models\Thread');
    }
}
