<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_type_id', 'category_id', 'role_id', 'user_id', 'title', 'description', 'is_opened', 'is_deleted',
    ];

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function threadType()
    {
        return $this->belongsTo('App\Models\Thread');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
