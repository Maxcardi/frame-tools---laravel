<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message_id', 'thread_id', 'user_id', 'body', 'is_deleted',
    ];

    public function messages()
    {
        return Message::all()->where('message_id', $this->id);
    }

    public function thread()
    {
        return $this->belongsTo('App\Models\Thread');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
