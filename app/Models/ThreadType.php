<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThreadType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'label', 'value',
    ];

    public function threads()
    {
        return $this->hasMany('App\Models\Thread');
    }
}
