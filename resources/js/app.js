require("./bootstrap");
import router from "./router.js";
import auth from "./auth/auth.js";
import bootstrap from "./bootstrap";

window.Vue = require("vue");
window.auth = auth;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context("./", true, /\.vue$/i);
files.keys().map(key =>
    Vue.component(
        key
            .split("/")
            .pop()
            .split(".")[0],
        files(key).default
    )
);
console.log(axios.defaults.headers.common);
axios.defaults.baseURL = process.env.MIX_APP_URL;
axios.defaults.headers.common["Authorization"] =
    "Bearer " + window.localStorage.token;
console.log(axios.defaults.headers.common);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router
}).$mount("#app");
