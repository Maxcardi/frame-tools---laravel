class Auth {
    /**Class constructor*/
    constructor() {
        this.token = null;
    }

    /** Put the token in the local storage
     * to keep the user logged in*/
    login(token) {
        window.localStorage.token = token;
        this.token = token;
    }

    /**Delete the token from the local storage
     * to on the logout*/
    logout() {
        window.localStorage.removeItem("token");
        this.token = null;
    }

    /**Check if the user is connected
     */
    check() {
        if (window.localStorage.token) {
            this.login(window.localStorage.token);
            return true;
        } else {
            return !!this.token;
        }
    }
}

export default new Auth();
