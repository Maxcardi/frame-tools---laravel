import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

import Home from './components/pages/Home.vue'
import Outils from './components/pages/outils/outils.vue'

import Forum from './components/pages/categories/Index.vue'
import CategoryShow  from './components/pages/categories/CategoryShow.vue'
import ThreadShow from './components/pages/categories/ThreadShow.vue'

const routes = [
    { name: "home", path: '/', component: Home },

    { name: "outils", path: '/outils', component: Outils},

    { name: "forum", path: '/forum', component: Forum },

    { name: "category.show", path: '/category/:id', component: CategoryShow },

    { name: "thread.show", path: '/thread/:id', component: ThreadShow },
]

export default new Router({
    routes,
});