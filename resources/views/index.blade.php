<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="https://kit.fontawesome.com/14f88d1257.js"></script>
        <link rel="stylesheet" href=" {{ asset('css/bootstrap.min.css') }} ">
        <link rel="stylesheet" href=" {{ asset('css/style.css') }} ">
        <title> {{ config('app.name') }} </title>
    </head>
    <body class="h-100">
        <div class="container-fluid" id="app">
                <main class="main-content">
                    <div class="row">
                        <div class="col-lg-2 p-0">
                            <Sidebar></Sidebar>
                        </div>
                        <div class="col-lg-10 p-0">
                            <Navbar></Navbar>
                            <div class="main-content-container container-fluid p-0">
                                <transition name="slide-fade" mode="out-in">
                                    <router-view></router-view>
                                </transition>
                            </div>
                        </div>
                    </div>
                </main>
            
        </div>
            
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>