<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| Public routes
|--------------------------------------------------------------------------
*/
Route::group([], function () {
    /* 
    |-------------------------------------------------------------------------- 
    | Auth route 
    |-------------------------------------------------------------------------- 
    */
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    /* 
    |-------------------------------------------------------------------------- 
    | Thread route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::get('threads', 'ThreadController@index'); 
    Route::get('thread/{thread}/messages', 'ThreadController@messages'); 
    Route::get('thread/{thread}', 'ThreadController@show'); 

    /* 
    |-------------------------------------------------------------------------- 
    | ThreadType route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::get('threadtypes', 'ThreadTypeController@index'); 
    Route::get('threadtype/{threadtype}/threads', 'ThreadTypeController@threads'); 
    Route::get('threadtype/{threadtype}', 'ThreadTypeController@show'); 

    /* 
    |-------------------------------------------------------------------------- 
    | Category route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::get('categories', 'CategoryController@index');  
    Route::get('category/{category}/threads', 'CategoryController@threads'); 
    Route::get('category/{category}', 'CategoryController@show'); 

    /* 
    |-------------------------------------------------------------------------- 
    | Message route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::get('messages', 'MessageController@index'); 
    Route::get('message/{message}/messages', 'MessageController@messages'); 
    Route::get('message/{message}', 'MessageController@show'); 

    /*
    |--------------------------------------------------------------------------
    | Role route
    |--------------------------------------------------------------------------
    */
    Route::get('roles', 'RoleController@index');
    Route::get('role/{role}', 'RoleController@show');

    /*
    |--------------------------------------------------------------------------
    | User route
    |--------------------------------------------------------------------------
    */
    Route::get('users', 'UserController@index');
    Route::get('user/{user}', 'UserController@show');
});

/*
|--------------------------------------------------------------------------
| Private routes
|--------------------------------------------------------------------------
*/
Route::group([
    'middleware' => 'auth:api',
], function () {
    /* 
    |-------------------------------------------------------------------------- 
    | Auth route 
    |-------------------------------------------------------------------------- 
    */
    Route::post('logout', 'AuthController@logout');

    /* 
    |-------------------------------------------------------------------------- 
    | Thread route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::post('thread', 'ThreadController@store'); 
    Route::put('thread/{thread}', 'ThreadController@update'); 
    Route::delete('thread/{thread}', 'ThreadController@delete');

    /* 
    |-------------------------------------------------------------------------- 
    | ThreadType route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::post('threadtype', 'ThreadTypeController@store'); 
    Route::put('threadtype/{threadtype}', 'ThreadTypeController@update'); 
    Route::delete('threadtype/{threadtype}', 'ThreadTypeController@delete'); 

    /* 
    |-------------------------------------------------------------------------- 
    | Category route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::post('category', 'CategoryController@store');
    Route::put('category/{category}', 'CategoryController@update'); 
    Route::delete('category/{category}', 'CategoryController@delete'); 

    /* 
    |-------------------------------------------------------------------------- 
    | Message route 
    |-------------------------------------------------------------------------- 
    */ 
    Route::post('message', 'MessageController@store'); 
    Route::put('message/{message}', 'MessageController@update'); 
    Route::delete('message/{message}', 'MessageController@delete'); 

    /*
    |--------------------------------------------------------------------------
    | Role route
    |--------------------------------------------------------------------------
    */
    Route::post('role', 'RoleController@store');
    Route::put('role/{role}', 'RoleController@update');
    Route::delete('role/{role}', 'RoleController@destroy');

    /*
    |--------------------------------------------------------------------------
    | User route
    |--------------------------------------------------------------------------
    */
    Route::post('user', 'UserController@store');
    Route::put('user/{user}', 'UserController@update');
    Route::delete('user/{user}', 'UserController@destroy');
});
